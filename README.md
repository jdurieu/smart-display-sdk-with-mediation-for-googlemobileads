# GoogleMobileAdsMediation

A description of this package.

Currently SmartAd doesn't provide GoogleMobileAdsMediation SDK as Swift package manager. 
This repository is an alternate version to use the SDK with Swift package manager. 
The Framework is updated directly from the original library, ensuring security.

## Curent Version
- GoogleMobileAdsMediation: 7.18.0.1

Available _subspecs_ are:

| Subspec name | Supported SDK version | Comments |
| ------------ | --------------------- | -------- |
| ```GoogleMobileAds``` | ~> 9.1.0 | _n/a_ |
