// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GoogleMobileAdsMediation",
    platforms: [
        .iOS(.v10)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "GoogleMobileAdsMediation",
            targets: ["GoogleMobileAdsMediationTarget"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(name:"SCSCoreKit",
                 url: "https://gitlab.com/jdurieu/smart_core_sdk_spm",
                 from: "7.18.1"),
        .package(name:"SASDisplayKit",
                 url: "https://gitlab.com/jdurieu/smart_display_sdk_spm",
                 from: "7.18.0"),
        .package(name:"GoogleMobileAds",
                 url: "https://github.com/googleads/swift-package-manager-google-mobile-ads",
                 from: "9.9.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "GoogleMobileAdsMediationTarget",
            dependencies: [
                           .product(name: "SCSCoreKit", package: "SCSCoreKit"),
                           .product(name: "SASDisplayKit", package: "SASDisplayKit"),
                           .product(name: "GoogleMobileAds", package: "GoogleMobileAds"),
            ]
        ),
    ]
)
